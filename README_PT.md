# NSD - NLnet Labs Name Server Daemon

## Introdução

O [NSD](https://nlnetlabs.nl/projects/nsd/about/) é um servidor de DNS open source, autoritativo eficiente e bem simples de configurar. Da mesma forma que com o Unbound, o NSD é desenvolvido pela [NLnet Labs](https://nlnetlabs.nl/) e foi desenvolvido com o propósito de ser apenas um name server autoritativo, podendo ser usado em conjunto com outro caching DNS, como o [Unbound](https://nlnetlabs.nl/projects/unbound/about/), sendo um excelente par para uma solução completa de DNS.

O NSD utiliza arquivos de zona no estilo do [BIND](https://www.isc.org/bind/). Durante a execução os arquivos são compilados para um arquivo de banco de dados binário que permite que o NSD tenha uma execução mais eficiente

Por fim, este tutorial pode ser utilizado como um standalone para configuração do NSD, como também pode ser usado em conjunto com meu [tutorial anterior de Unbound](https://codeberg.org/cgoslaw/unbound) - ambos podendo ser utilizados em um mesmo sistema ou em servidores diferentes para oferecer mais segurança.

## Pre-setup

Neste guia, farei a configuração de dois servidores NSD - um master e um slave, onde ambos estão em uma subnet separada do meu caching DNS (Unbound) e dos meus outros sistemas. Sendo assim, toda query de DNS vai primeiro para meu servidor unbound (10.1.1.127) que decide se a query deverá ser feita localmente nos meus servidores NSD (10.2.2.112 e 10.2.2.224) a partir do endereço e FQDN, ou direto para a internet.

A configuração do NSD segue o mesmo padrão da configuração do Unbound, ambos tendo uma configuração bem simples.

Assim como na descrição, a configuração do NSD será feita também no [OBSD 6.7](https://www.openbsd.org/) e como sempre, indico a leitura das man pages para uma configuração mais apropriada para seu caso. Sendo assim, por favor atente-se aos locais de configuração e passos referentes ao seu sistema.

Por fim, estarei usando a porta 53530 para queries, simplesmente para fazer com que na minha rede toda request para porta 53 seja feita no meu servidor rodando Unbound. Não é necessário fazer essa mudança.

## Setup

No OBSD, o NSD já vem pre-instalado no sistema base, então não há necessidade de instalação e adição do user relacionado. Caso sua situação seja diferente, instale o NSD e crie um usuário para o NSD se for necessário.

Em uma infraestrutura onde há servidores slave, é necessário criar uma chave segura para transferência das zonas do master para os servidores slave. Esta chave será configurada no nsd.conf de todos os servidores.
Você pode gerar uma key com o seguinte comando:

```
$ dd if=/dev/random count=1 bs=32 2> /dev/null | base64
```
Também, vamos gerar alguns certificados para que possamos utilizar a ferramenta [nsd-control(8)](https://man.openbsd.org/man8/nsd-control.8) para que possamos administrar nosso serviço do NSD. Por default, o NSD escuta no localhost e utiliza TLS sobre TCP, onde servidor e client se autenticam os esses certificados self-signed. Para gerá-los, usarei o [nsd-control-setup](https://man.openbsd.org/man8/nsd-control.8):
```
$ doas nsd-control-setup
```
Agora podemos iniciar a configuração do master server editando o [nsd.conf(5)](https://man.openbsd.org/man5/nsd.conf.5). Farei backup da configuração default e farei uma nova:
```
$ doas mv /var/nsd/etc/nsd.conf /var/nsd/etc/.nsd.conf.backup
$ doas vim /var/nsd/etc/nsd.conf
```
A primeira seção, é a seção "server" que especifica opções globais e locais de arquivos:
```
# ---------------------------------- #
# --- NSD Authoritative only DNS --- #
# ---------------------------------- #

server:
	server-count: 1
	ip-address: 10.2.2.112
	ip-transparent: no
	do-ip4: yes
	do-ip6: no
	port: 53530
	
	chroot: "/var/nsd"
	zonesdir: "/var/nsd/zones"
	zonelistfile: "/var/nsd/db/zone.list"
	database: "/var/nsd/db/nsd.db"
	logfile: "/var/nsd/log/nsd.log"
	pidfile: "/var/nsd/run/nsd.pid"

	identity: "example.net intranet nameserver"	
	log-time-ascii: yes
	verbosity: 9
	hide-version: yes
```
Agora faremos a configuração da seção "remote-control" que é usada para definir as opções para usar a ferramenta nsd-control que permite executar comandos ao NSD server:
```
remote-control:
	control-enable: yes
	control-interface: 127.0.0.1
	control-port: 8952
	server-key-file: "/var/nsd/nsd_server.key"
	server-cert-file: "/var/nsd/nsd_server.pem"
	control-key-file: "/var/nsd/nsd_control.key"
	control-cert-file: "/var/nsd/nsd_control.pem"
```
A seção "key" serve para definirmos a key que será utilizada para que o master notifique os servidores slave:
```
key:
	name: "sec_key"
	algorithm: hmac-sha256
	secret: "J8M6HIid8XVDz2NJyc7sKldytF2OiNfKozPj43lUgD4="
```
E agora definimos as zonas pelas quais seremos autoridade e daremos respostas caso hajam queries nessas subnets/fqdns
```
zone:
	name: "example.net"
	zonefile: "zone.example.net"
	notify: 10.2.2.224@53530 sec_key
	provide-xfr: 10.2.2.224 sec_key

zone:
	name: "0.168.192.in-addr.arpa"
	zonefile: "zone.0.168.192"
	notify: 10.2.2.224@53530 sec_key
	provide-xfr: 10.2.2.224 sec_key
zone:
	name: "1.1.10.in-addr.arpa"
	zonefile: "zone.1.1.10"
	notify: 10.2.2.224@53530 sec_key
	provide-xfr: 10.2.2.224 sec_key
zone:
	name: "2.2.10.in-addr.arpa"
	zonefile: "zone.2.2.10"
	notify: 10.2.2.224@53530 sec_key
	provide-xfr: 10.2.2.224 sec_key
```
E isso é tudo que é necessário para configuração do nsd.conf no servidor master.

Podemos checar se cometemos algum erro com o [nsd-checkconf](https://man.openbsd.org/man8/nsd-checkconf.8):
```
$ doas nsd-checkconf
```
## Configuração de zonas
Agora partirmos para nossos arquivos de configuração de zonas, que são responsáveis tanto por forward queries quanto por reverse queries.

**Zone file de forward queries**
Caso não queira/precise de reverse queries, este é o único arquivo necessário:
```
$ vim /var/nsd/zones/zone.example.net
```
```
$ORIGIN example.net.
$TTL 1800

@	IN	SOA	ns01.example.net.	hostmaster.example.net. (
			2020111701	; Serial Number
			43200		; Refresh
			3600		; Retry
			864000		; Expire
			86400		; Min TTL
)
			
;; ----------------------------------
;; NS & MX
		NS	ns01.example.net.
		NS	ns02.example.net.
		MX	0 mx01.example.net.
;; ----------------------------------
;; Servers
ns01	IN	A	10.2.2.112
ns02	IN	A	10.2.2.224
un01	IN	A	10.1.1.127
ldap	IN  A   192.168.0.248
mx01	IN	A	192.168.0.249
void	IN	A	192.168.0.34
;; ----------------------------------
;; Gateway
firewall	IN	A	192.168.0.156
firewall	IN	A	10.1.1.156
firewall	IN	A	10.2.2.156
;; ----------------------------------
```
**Zone file reverso para os endereços na subnet 192.168.0.0/24**
```
$ vim /var/nsd/zones/zone.0.168.192
```
```
$ORIGIN example.net.
$TTL 86400

0.168.192.in-addr.arpa.	IN SOA	ns01.example.net. hostmaster.example.net. (
	2020111201	;	Serial Number
	43200		;	Refresh
	7200		;	Retry
	86400		;	Expire
	86400		;	Min TTL
)

156.0.168.192.in-addr.arpa. IN PTR firewall
34.0.168.192.in-addr.arpa. IN PTR void
248.0.168.192.in-addr.arpa. IN PTR ldap
249.0.168.192.in-addr.arpa. IN PTR mx01
```
**Zone file reverso para os endereços na subnet 10.1.1.0/24**
```
$ vim /var/nsd/zones/zone.1.1.10
```
```
$ORIGIN example.net.
$TTL 86400

1.1.10.in-addr.arpa.	IN SOA	ns01.example.net. hostmaster.example.net (
	2020111201	;	Serial Number
	43200		;	Refresh
	7200		;	Retry
	86400		;	Expire
	86400		;	Min TTL
)

127.1.1.10.in-addr.arpa. IN	PTR	un01
156.1.1.10.in-addr.arpa. IN	PTR	firewall
```
**Zone file reverso para os endereços na subnet 10.2.2.0/24**
```
$ vim /var/nsd/zones/zone.2.2.10
```
```
$ORIGIN example.net.
$TTL 86400

2.2.10.in-addr.arpa.	IN SOA	ns01.example.net. hostmaster.example.net. (
	2020111201	;	Serial Number
	43200		;	Refresh
	7200		;	Retry
	86400		;	Expire
	86400		;	Min TTL
)

112.2.2.10.in-addr.arpa.	IN	PTR	ns01
224.2.2.10.in-addr.arpa.	IN	PTR	ns02
156.2.2.10.in-addr.arpa.	IN	PTR	firewall
```
Podemos verificar se nossa configuração das zonas foram feitas corretamente utilizando a ferramenta nsd-checkzone:
```
nsd-checkzone example.net /var/nsd/zones/zone.example.net
zone example.net is ok
```
##  Configuração do servidor Slave
Podemos partir agora para a configuração do nsd.conf do servidor slave que segue o mesmo padrão, porém com algumas diferenças nas configurações das zonas:
```
# ------------------------------------------ #
# --- NSD Authoritative only DNS - Slave --- #
# ------------------------------------------ #

server:
	server-count: 1
	ip-address: 10.2.2.224
	ip-transparent: no
	do-ip4: yes
	do-ip6: no
	port: 53530

	chroot: "/var/nsd"
	zonesdir: "/var/nsd/zones/slave"
	zonelistfile: "/var/nsd/db/zone.list"
	database: "/var/nsd/db/nsd.db"
	logfile: "/var/nsd/log/nsd.log"
	pidfile: "/var/nsd/nsd.pid"

	identity: ""
	hide-identity: yes
	version: ""
	hide-version: yes
	log-time-ascii: yes
	verbosity: 9

remote-control:
	control-enable: yes
	control-interface: 10.2.2.224
	control-port: 8952
	server-key-file: "/var/nsd/nsd_server.key"
	server-cert-file: "/var/nsd/nsd_server.pem"
	control-key-file: "/var/nsd/nsd_control.key"
	control-cert-file: "/var/nsd/nsd_control.pem"

key:
        name: "sec_key"
        algorithm: hmac-sha256
        secret: "J8M6HIid8XVDz2NJyc7sKldytF2OiNfKozPj43lUgD4="
zone:
	name: "example.net"
	zonefile: "zone.example.net"
	allow-notify: 10.2.2.112 sec_key
	request-xfr: AXFR 10.2.2.112@53530 sec_key
zone:
	name: "0.168.192.in-addr.arpa"
	zonefile: "zone.0.168.192"
	allow-notify: 10.2.2.112 sec_key
	request-xfr: AXFR 10.2.2.112@53530 sec_key
zone:
	name: "1.1.10.in-addr.arpa"
	zonefile: "zone.1.1.10"
	allow-notify: 10.2.2.112 sec_key
	request-xfr: AXFR 10.2.2.112@53530 sec_key
	
zone:
	name: "2.2.10.in-addr.arpa"
	zonefile: "zone.2.2.10"
	allow-notify: 10.2.2.112 sec_key
	request-xfr: AXFR 10.2.2.112@53530 sec_key
```
Agora podemos checar se há algum erro de configuração com o nsd-checkconf:
```
$ nsd-checkconf
```
## Finalização
Com as configurações feitas, podemos subir serviço do NSD em ambos os servidores:
```
# rcctl enable nsd
# rcctl start nsd
```
E podemos ver se conseguimos notificar nosso servidor slave das nossas zonas:
```
# nsd-control notify
ok
```
Verificando o log, podemos ver um pouco mais de verbosity sobre o que acabamos de fazer:
```
$ tail /var/nsd/log/nsd.log
```
```
[2020-11-26 15:51:20.118] nsd[44687]: info: new control connection from 10.2.2.112
[2020-11-26 15:51:20.171] nsd[44687]: info: remote control connection authenticated
[2020-11-26 15:51:20.171] nsd[44687]: info: control cmd:  notify
[2020-11-26 15:51:20.172] nsd[44687]: info: remote control operation completed
[2020-11-26 15:51:20.191] nsd[2518]: info: axfr for 1.1.10.in-addr.arpa. from 10.2.2.224
[2020-11-26 15:51:20.199] nsd[2518]: info: axfr for example.net. from 10.2.2.224
[2020-11-26 15:51:20.200] nsd[2518]: info: axfr for 0.168.192.in-addr.arpa. from 10.2.2.224
[2020-11-26 15:51:20.208] nsd[2518]: info: axfr for 2.2.10.in-addr.arpa. from 10.2.2.224
```
Agora podemos verificar se conseguimos fazer queries direto no nosso slave server:
```
$ dig -p 53530 firewall.example.net @10.2.2.224

; <<>> dig 9.10.8-P1 <<>> -p 53530 firewall.example.net @10.2.2.224
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 63371
;; flags: qr aa rd; QUERY: 1, ANSWER: 3, AUTHORITY: 2, ADDITIONAL: 3
;; WARNING: recursion requested but not available

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
firewall.example.net.             IN      A

;; ANSWER SECTION:
firewall.example.net.      1800    IN      A       192.168.0.156
firewall.example.net.      1800    IN      A       10.1.1.156
firewall.example.net.      1800    IN      A       10.2.2.156

;; AUTHORITY SECTION:
example.net.                1800    IN      NS      ns01.example.net.
example.net.                1800    IN      NS      ns02.example.net.

;; ADDITIONAL SECTION:
ns01.example.net.           1800    IN      A       10.2.2.112
ns02.example.net.           1800    IN      A       10.2.2.224

;; Query time: 0 msec
;; SERVER: 10.2.2.224#53530(10.2.2.224)
;; WHEN: Thu Nov 26 15:56:06 -03 2020
;; MSG SIZE  rcvd: 164
```
Agora um reverse query:
```
$ dig -p 53530 -x 192.168.0.156 @10.2.2.224 

; <<>> dig 9.10.8-P1 <<>> -p 53530 -x 192.168.0.156 @10.2.2.224
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 46778
;; flags: qr aa rd; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1
;; WARNING: recursion requested but not available

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;156.0.168.192.in-addr.arpa.    IN      PTR

;; ANSWER SECTION:
156.0.168.192.in-addr.arpa. 86400 IN    PTR     firewall.example.net.

;; Query time: 0 msec
;; SERVER: 10.2.2.224#53530(10.2.2.224)
;; WHEN: Thu Nov 26 16:00:27 -03 2020
;; MSG SIZE  rcvd: 86
```
## Considerações Finais
E assim podemos ver que configuramos tudo corretamente e o NSD está rodando em ambos servidores - master e slave. 
Quando alguma alteração precisar ser feita, basta fazer alguma modificação nas zonas que precisarem ser alteradas e também modificar o serial number conforme a data e versão. Assim, basta fazer um reload das zonas e notificar os servidores slave caso os tenha e todos já estão atualizados com as últimas alterações.

Se você tiver algum comentário, correção ou alterações que considere apropriadas, por favor sinta-se à vontade em me contatar.

Este tutorial e arquivos estão disponíveis em meu repostório:
[Codeberg - CGoslaw](https://codeberg.org/cgoslaw)

## Recomendações
[NSD, NLnet Labs](https://nlnetlabs.nl/projects/nsd/about/)
[nsd(8)](https://man.openbsd.org/man8/nsd.8)
[nsd.conf(5)](https://man.openbsd.org/man5/nsd.conf.5)
[nsd-control(8)](https://man.openbsd.org/man8/nsd-control.8)

